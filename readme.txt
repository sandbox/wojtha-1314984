CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Permissions
 * Usage
 * Sponsors

INTRODUCTION
------------

Current Maintainers:

 * Beng Tan <http://drupal.org/user/132729>
 * Devin Carlson <http://drupal.org/user/290182>

Submenu Tree provides a method for structuring content hierarchically. For 
content which has a menu link, Submenu Tree display a list of content which is 
at the same level or below the content in the menu.

Content that at the same level as a selected piece of content is referred to 
as "sibling" content while content that is below a selected piece of content 
is referred to as "sub" content.

Lists of sub or sibling content can be displayed either in a block or directly 
inside of the body of a piece of content. Lists can be formatted in six 
different ways:

 * Menu
 * Titles only
 * Teasers
 * Teasers with links
 * Full text
 * Full text with links

Submenu Tree also provides a block of "extended" main links. The extended main 
links are made up of all menu links in the Main links menu (or Secondary links 
menu if the Main links menu does not exist) that are two or more levels deep.

INSTALLATION
------------

The Menu module included with Drupal is required in order to install Submenu 
Tree.

Submenu Tree can be installed via the standard Drupal installation process 
(http://drupal.org/node/895232).

PERMISSIONS
------------

Once a user role has access to sub or sibling menus, Submenu Tree becomes 
immediately useful.

Submenu Tree provides three permissions:

 * Administer submenu tree
    Allows a user to configure the display of sub or sibling content and the 
    default options when editing content.
 * Administer submenus
    Allows a user to edit sub content options when editing content.
 * Administer Siblingmenus
    Allows a user to edit sibling content options when editing content.

USAGE
-----

As an Administrator or a user with either the Administer submenus or 
Administer Siblingmenus permissions, you will see a vertical tab titled 
"Submenu tree settings" while editing content.

Selecting the tab will display options for enabling sub and/or sibling 
content, depending on user permissions.

Selecting either option will enable the display of sub/sibling content for the 
current content and expand the number of options presented. The additional 
options include a title textfield, a display select box and a weight select 
box. Note that both sub and sibling content can have unique titles, display 
settings and weights.

Each option has an impact on the display of sub and/or sibling content:

 * Title
    Optionally specify a title display above the sub/sibling content list or 
    as the sub/sibling content block title.
 * Display
    Selects where and how the sub/sibling content will be displayed. When 
    displaying sub/sibling content in a block, ensure that the respective 
    block(s) are enabled.
 * Weight
    Selects where in the page the sub/sibling content will appear when one of 
    the "content" display types has been selected.

Sponsors
--------

Development of Submenu Tree is sponsored by ThinkLeft (http://thinkleft.com) 
and the Ontario Ministry of Northern Development, Mines and Forestry 
(http://www.mndmf.gov.on.ca).
