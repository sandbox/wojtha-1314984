<?php
/**
 * @file
 * Submenu Tree configuration options.
 */

function submenutree_configuration_form($form, &$form_state) {
  $form = array();

  $types = node_type_get_types();
  foreach($types as $node_type) {
    $content_type_options[$node_type->type] = $node_type->name;
  }

  $display_options = _submenutree_display_options();

  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#description' => t('Configure general Submenu Tree settings.'),
  );

  $form['general_settings']['submenutree_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed content types'),
    '#options' => $content_type_options,
    '#default_value' => variable_get('submenutree_node_types', array('page')),
    '#description' => t('A Submenutree widget will be available on these content types.'),
  );

  $form['general_settings']['block_title'] = array(
    '#type' => 'radios',
    '#title' => t('Block title generation'),
    '#description' => t("Choose how the title of the sub and sibling blocks is generated when the content's title has not been set."),
    '#default_value' => variable_get('block_title', 'current_title'),
    '#options' => array(
      'current_title' => t("Use the current content's title."),
      'first_level_menu_parent' => t("Use the title of the current content's first-level menu parent.")
    ),
  );

  $form['submenutree'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default settings'),
    '#description' => t('Choose the default Submenu Tree settings to display when editing content.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['submenutree']['submenutree_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sub content'),
  );

  $form['submenutree']['submenutree_container']['submenutree_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('submenutree_title', ''),
  );

  $form['submenutree']['submenutree_container']['submenutree_display'] = array(
    '#type' => 'select',
    '#title' => t('Display type'),
    '#options' => $display_options,
    '#default_value' => variable_get('submenutree_display', 'menu'),
  );

  $form['submenutree']['submenutree_container']['submenutree_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => variable_get('submenutree_weight', 0),
  );

  $form['submenutree']['siblingmenutree_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sibling content'),
  );

  $form['submenutree']['siblingmenutree_container']['siblingmenutree_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('siblingmenutree_title', ''),
  );

  $form['submenutree']['siblingmenutree_container']['siblingmenutree_display'] = array(
    '#type' => 'select',
    '#title' => t('Display type'),
    '#options' => $display_options,
    '#default_value' => variable_get('siblingmenutree_display', 'menu'),
  );

  $form['submenutree']['siblingmenutree_container']['siblingmenutree_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => variable_get('siblingmenutree_weight', 0),
  );

  return system_settings_form($form);
}
